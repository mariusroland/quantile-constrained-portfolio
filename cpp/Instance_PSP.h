
#pragma once

#include "Common.h"

using namespace std;

class Instance_PSP {

	//ATTRIBUTES
	/**
	* int asset number
	* int scenario number
	* int scenario number reduced
	* vector<<vector<<double>> returns 
	* vector<<vector<<double>> returns reduced 
	* vector<double> probabilities
	* vector<double> probabilities reduced
	* vector<double> expectedReturns
	* double alpha
	* double rho
	* vector<double> bigMs
	* vector<double> bigMs reduced maybe i dont need
	* double tau
	* vector<double> weakVIcoefficients
	* vector<int> clusteredScenarios
	* bool weakVI
	* bool strongVI
	* bool noVI
	* bool isReduced
	* double timeLimit
	* 
	* build instance in a different constructor
	* 
	* functions:
	* readFiles main
	* readAssetScenarioNumbers
	* readReturns
	* computeBigMs (use getters to compute the reduced/normal ones)
	* computeVIcoeffs (if bool) 
	* createReduced (if reduced is true, bool isMinAvg, int clusteringNumber, vector<int> clusteredScenarios) -> compute all the reduced attributes given min/avg
	* computeReducedStrongVI(vector<bool> scenarioSet) return vector of b_i	 call this inside createReduced if isMinAvg is true
	* getters only necessary (with boolean)
	* computeRealQuantile(vector<int> x) return Q
	* computeRealRisk(vector<int> x) return vector<double> rr; rr[s] = risk associate to scenario s, s in original scenario
	*/

	//path and instance name
	string path;
	string name;

	/**
	*	Asset number
	*	scenario number 
	*	rho = minimum return
	*	tau = quantile
	*	scenariosToDeactivate = number of scenario to be deactivated
	*	alpha = obj function weight	
	*	returns 
	*	probabilities 
	*	big Ms
	*/
	int assetNumber;
	int scenarioNumber;
	double rho;
	double tau;
	int scenariosToDeactivate;
	double alpha;
	vector<vector<double>> returns;
	vector<double> expectedReturns;
	vector<double> probabilities;
	vector<double> bigMs;
	
	/**
	*	isReduced = true if clustering alg. is on
	*	scenario number reduced
	*	returns reduced
	*	probabilities reduced
	*	big Ms reduced
	*/
	bool isReduced;
	int scenarioNumberReduced;
	vector<vector<double>> returnsReduced;
	vector<double> probabilitiesReduced;

	/**
	*	isNoVI = true if no Vi are on
	*	areWeakVI = true if weak Vi are on
	*	areStrongVI = true if strong Vi are on
	*	weak VI coefficients
	*	if isReduced=true, compute the coefficients of strong VIs, for each cluster whose probability is strictly greater than tau 
	*		key = cluster id(scenario id in scenarioNumberReduced), value vector b of coefficients, b[i] coefficient of variable x[i]
	*/
	bool isNoVI;
	bool areWeakVI;
	bool areStrongVI;
	vector<double> weakVIcoeffs;
	unordered_map<int, vector<double>> strongVICoefficients;

	/**
	*	algorithm start time
	*/
	chrono::time_point<chrono::system_clock> startTime;

public:

	//cut violation and counter
	static int generatedCuts;
	static double minViolation;
	static double maxViolation;
	static double separationTime;
	static double rootNodeUB;
	static double rootNodeGAP;
	static double violationTime;

	//CONSTRUCTORS AND DESTRUCTORS	
	Instance_PSP();
	Instance_PSP(string path, string name, bool isReduced, bool areWeakVI, bool areStrongVI, double alpha, int scenariosToDeactivate);
	Instance_PSP(string name, int assetNumber, int scenarioNumber, double minReturn, double maxReturn);															//call the instance generator
	Instance_PSP(const Instance_PSP& instance);
	~Instance_PSP();

	//METHODS

	//Procedures to read the instance file and display the data
	void readFile();
	void readAssetScenarioNumber();
	void readReturns();
	void computeWeakVICoefficients();

	/**
	* @param x = wealth allocation to the securities
	* @return vector r of size scenarioNumber, where r[s] = sum_{i in asset} x[i]*returns[i][s] = return at scenario s
	*/
	vector<double> computeRealReturnPerScenario(const vector<double>& x);
	vector<double> computeReducedReturnPerScenario(const vector<double>& x);
	/**
	* call computeRealReturnPerScenario to get the return per scenarios r 
	* sort r
	* @param x = wealth allocation to the securities
	* @return tau-quantile of r
	*/
	double computeRealQuantile(const vector<double>& x);
	double computeReducedQuantile(const vector<double>& x);
	/**
	* call computeRealQuantile 
	* @param x = wealth allocation to the securities
	* @return objective value
	*/
	double computeRealObjective(const vector<double>& x);
	vector<double> computeYVariables(const vector<double>& x);
	double computeReducedObjective(const vector<double>& x);

	/**
	* compute the reduced instance: scenarioNumberReduced, returnsReduced, probabilitiesReduced
	* @param isMaxAvg = true if min is on, false if avg is on
	* @param clusterNumber = number of clusters. clusters go from 0 to clusterNumber-1
	* @param clusteredScenarios = vector c of int of size scenarioNumber. c[s] is the cluster to which scenario s belong
	*/
	void buildReducedInstance(const bool& isMaxAvg, const int& clusterNumber, const vector<int>& clusteredScenarios);
	void computeAvgReturnClusters(const vector<vector<int>>& scenariosPerCluster);
	void computeMaxReturnClusters(const vector<vector<int>>& scenariosPerCluster);
	/**
	* compute the coefficients of the strong VI, for each cluster whose probability is strictly greater than tau
	*/
	void computeStrongVIcoeffs(const vector<vector<int>>& scenariosPerCluster);
	vector<double> computeCoeffs(const int& scenario, const vector<int>& cluster);
	
	//GET-SET ATTRIBUTES
	string getName();
	int& getAssetNumber();
	int& getScenarioNumber(bool forceReal = false);
	double& getRho();
	double& getTau();
	double& getAlpha();
	vector<vector<double>>& getReturns(bool forceReal = false);
	vector<double>& getExpectedReturns();
	vector<double>& getProbabilities(bool forceReal = false);
	vector<double>& getBigMs();

	bool getIsReduced();
	
	bool& getIsNoVI();
	bool& getAreWeakVI();
	bool& getAreStrongVI();
	vector<double>& getWeakVIcoeffs();
	unordered_map<int, vector<double>>& getStrongVICoefficients();

	const chrono::time_point<chrono::system_clock>& getStartTime() const;

	void setBigMs();
	void setIsReduced(const bool& isReduced);

	/**
	* randomly generate asset returns between minReturn and maxReturn 
	*/
	void WolseyInstanceGenerator(string name, int assetNumber, int scenarioNumber, double minReturn, double maxReturn);

};
