#include "ModelSolver_PSP.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

ModelSolver_PSP::ModelSolver_PSP(Instance_PSP* instance, double timeLimit) {
	this->instance = instance;
	this->timeLimit = timeLimit;
	this->gap = 10e-10;
	this->milpModel = new MilpModel_PSP(this->instance);
}

ModelSolver_PSP::~ModelSolver_PSP() {
	delete this->milpModel;
}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void ModelSolver_PSP::solve(const bool& isLogFilePrint) {

	this->milpModel->buildModel();				
	this->milpModel->initializeCallback();
	this->milpModel->solveModel(this->timeLimit, this->gap, isLogFilePrint);
	vector<double> x = this->milpModel->getXValues();
	cout<<"Real Objective: "<<this->instance->computeRealObjective(x)<<endl;
	this->milpModel->extractSolutionAttributes();

}

void ModelSolver_PSP::writeStatistics(string fileName) {

	ofstream mystream;
	mystream.open(fileName, ios::app);

	mystream << this->instance->getName() << "," << this->instance->getAssetNumber() << "," << this->instance->getScenarioNumber() << "," << this->instance->getAlpha() << "," << this->instance->getRho() << "," << this->instance->getTau() << "," << this->milpModel->getModelBaBNodes() << "," << this->milpModel->getModelTime() << "," << this->milpModel->getModelObjectiveValue() << "," << this->milpModel->getModelObjectiveBound() << "," << this->milpModel->getModelMipGap() << "," << this->instance->getAreWeakVI() << "," << this->instance->getAreStrongVI() << "," << Instance_PSP::generatedCuts << "," << Instance_PSP::separationTime << "," << Instance_PSP::rootNodeUB << "," << Instance_PSP::rootNodeGAP << "\n";

	mystream.close();

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////

const double& ModelSolver_PSP::getTimeLimit() const {

	return this->timeLimit;

}
