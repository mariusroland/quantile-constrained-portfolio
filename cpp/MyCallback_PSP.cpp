#include "MyCallback_PSP.h"

///////////////////////CONSTRUCTORS AND DESTRUCTORS//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

MyCallback_PSP::MyCallback_PSP(Instance_PSP* instance, GRBModel& model_, vector<GRBVar>& x_, vector<GRBVar>& y_, GRBVar& Q_)
	: model(model_), x(x_), y(y_), Q(Q_) {
  this->instance = instance;
  this->xNodeSolution = vector<double>(this->instance->getAssetNumber());
  this->yNodeSolution = vector<double>(this->instance->getScenarioNumber());
  this->QNodeSolution = -1;
}

MyCallback_PSP::MyCallback_PSP(const MyCallback_PSP& myCallback_PSP)
  : model(myCallback_PSP.model), x(myCallback_PSP.x), y(myCallback_PSP.y), Q(myCallback_PSP.Q) {
  this->instance = myCallback_PSP.instance;
}

MyCallback_PSP::~MyCallback_PSP() {

}

///////////////////////////METHODS//////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////exclusionsCallback//////////////////////////////////////////

void MyCallback_PSP::callback() {
	chrono::time_point<chrono::system_clock> sepStart, sepEnd;

	if (!this->instance->getIsNoVI()) {

		//VALID INEQUALITIES
		if (where == GRB_CB_MIPNODE && getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL && getDoubleInfo(GRB_CB_MIPNODE_NODCNT) == 0) {

			try {

				if (this->instance->getAreStrongVI()) {
					addVI22_violated_Jacques_strong();
				}
				else {
					addVI22_violated_Jacques();
				}

				double sepDuration = (chrono::duration_cast<chrono::nanoseconds>(sepEnd - sepStart).count()) * 1e-9;
				Instance_PSP::separationTime += sepDuration;

			}
			catch (GRBException& e) {

				cerr << "Concert exception caught: " << e.getErrorCode() << endl;
				cerr << e.getMessage() << endl;

			}
			catch (...) {

				cerr << "Unknown exception caught" << endl;

			}

		}

	}

	//root node info
	try {

		if (where == GRB_CB_MIPNODE && getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL && getDoubleInfo(GRB_CB_MIPNODE_NODCNT) == 0) {
			Instance_PSP::rootNodeUB = getDoubleInfo(GRB_CB_MIPNODE_OBJBND);
			double gap = ((getDoubleInfo(GRB_CB_MIPNODE_OBJBND) - getDoubleInfo(GRB_CB_MIPNODE_OBJBST)) / getDoubleInfo(GRB_CB_MIPNODE_OBJBST)) * 100;
			Instance_PSP::rootNodeGAP = gap;
		}

	}
	catch (GRBException& e) {

		cerr << "Concert exception caught: " << e.getErrorCode() << endl;
		cerr << e.getMessage() << endl;

	}
	catch (...) {

		cerr << "Unknown exception caught" << endl;

	}

}

//////////////////////////////setSetupTime////////////////////////////////////////////

void MyCallback_PSP::addVI22_violated_Jacques() {

	//chrono::time_point<chrono::system_clock> start, end;

	//get solution
	getNodeSolutionX_Jacques();
	getNodeSolutionQ_Jacques();
	
	vector<int> subset;
	double probabilitySubset = 0;

	//start = chrono::system_clock::now();
	if (isVI22_violated(subset, probabilitySubset)) {

		/*end = chrono::system_clock::now();
		double duration = (chrono::duration_cast<chrono::nanoseconds>	(end - start).count()) * 1e-9;
		Instance_PSP::violationTime += duration;*/

		GRBLinExpr constraint = 0;

		//BUILD THE ENTIRE CONSTRAINT IF THE SECOND TERM OF THE RHS IS ADDED
		for (int i = 0; i < this->instance->getAssetNumber(); i++) {

			double coeffAsset = this->instance->getWeakVIcoeffs().at(i);
			//BUILD THE SECOND TERM OF THE COEFFICIENT
			for (int j = 0; j < subset.size(); j++) {
				coeffAsset -= (this->instance->getProbabilities().at(subset.at(j)) * this->instance->getReturns().at(i).at(subset.at(j)));
			}

			constraint += coeffAsset * this->x[i];

		}

		constraint -= (1.0 - this->instance->getTau() - probabilitySubset) * this->Q;

		//ADD THE CONSTRAINT TO THE MODEL
		addLazy(constraint >= 0);
		//cout << constraint << endl;
		Instance_PSP::generatedCuts++;

	}

}

void MyCallback_PSP::addVI22_violated_Jacques_strong() {

	//chrono::time_point<chrono::system_clock> start, end;

	//get solution
	getNodeSolutionX_Jacques();
	getNodeSolutionQ_Jacques();
	vector<int> complement;
	double probabilitySubset = 0;

	vector<double> coefficients(this->instance->getAssetNumber(), INFINITY);

	//start = chrono::system_clock::now();
	if (isVI22_violated_strong(complement, probabilitySubset, coefficients)) {

		GRBLinExpr constraint = 0;

		//BUILD THE SECOND TERM OF THE RHS
		for (int i = 0; i < this->instance->getAssetNumber(); i++) {

			//double coeff = buildCoefficient(complement, probabilitySubset, i);
			constraint += coefficients.at(i) * this->x[i];

		}

		constraint -= (1.0 - this->instance->getTau() - probabilitySubset) * this->Q;
		addLazy(constraint >= 0);
		Instance_PSP::generatedCuts++;

	}

}

bool MyCallback_PSP::isVI22_violated(vector<int>& subset, double& probabilitySubset) {

	double constraintValue = 0;

	//BUILD THE SECOND TERM OF THE RHS
	for (int j = 0; j < this->instance->getScenarioNumber(); j++) {

		double secondTermValuePerScenario = -this->QNodeSolution;

		for (int i = 0; i < this->instance->getAssetNumber(); i++) {

			secondTermValuePerScenario += this->instance->getReturns().at(i).at(j) * this->xNodeSolution[i];

		}

		//if the term of the sum is positive->add it to the constraint
		if (secondTermValuePerScenario > epsilonValue) {
			secondTermValuePerScenario *= this->instance->getProbabilities().at(j);
			constraintValue -= secondTermValuePerScenario;
			subset.push_back(j);
			probabilitySubset += this->instance->getProbabilities().at(j);
		}

	}

	//BUILD THE ENTIRE CONSTRAINT IF THE SECOND TERM OF THE RHS IS ADDED
	if (constraintValue > -epsilonValue || probabilitySubset > 1.0 - this->instance->getTau() - epsilonValue) {
		return false;
	}

	//ADD THE FIRST TERM OF THE RHS
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {

		constraintValue += this->instance->getWeakVIcoeffs().at(i) * this->xNodeSolution[i];

	}

	//REMOVE THE QUANTILE
	double coeff = 1.0 - this->instance->getTau();
	constraintValue -= (coeff * this->QNodeSolution);

	//if the constraint is violated, ADD THE CONSTRAINT TO THE MODEL
	if (constraintValue < -epsilonValue) {
		return true;
	}
	return false;

}

bool MyCallback_PSP::isVI22_violated_strong(vector<int>& complement, double& probabilitySubset, vector<double>& coefficients) {

	double constraintValue = 0;

	//BUILD THE SECOND TERM OF THE RHS
	for (int j = 0; j < this->instance->getScenarioNumber(); j++) {

		double secondTermValuePerScenario = -this->QNodeSolution;

		for (int i = 0; i < this->instance->getAssetNumber(); i++) {

			secondTermValuePerScenario += this->instance->getReturns().at(i).at(j) * this->xNodeSolution[i];

		}

		//if the term of the sum is positive->add it to the constraint
		if (secondTermValuePerScenario > epsilonValue) {
			/*secondTermValuePerScenario *= this->instance->getScenarioProbabilities().at(j);
			constraintValue -= secondTermValuePerScenario;*/
			probabilitySubset += this->instance->getProbabilities().at(j);
		}
		else {
			complement.push_back(j);
		}

	}

	//BUILD THE ENTIRE CONSTRAINT IF THE SECOND TERM OF THE RHS IS ADDED
	if (/*constraintValue > -epsilonValue || */probabilitySubset > 1.0 - this->instance->getTau() - epsilonValue) {
		return false;
	}

	//ADD THE FIRST TERM OF THE RHS
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {

		coefficients.at(i) = buildCoefficient(complement, probabilitySubset, i);
		/*double coeff1 = computeVICoefficient(i, complement, probabilitySubset);
		cout << "COEFF " << coeff << " CHECK " << coeff1 << endl;
		cout << endl;*/
		constraintValue += coefficients.at(i) * this->xNodeSolution[i];
		//constraintValue += this->instance->getVIcoefficients().at(i) * this->xNodeSolution[i];

	}

	//REMOVE THE QUANTILE
	double coeff1 = 1.0 - this->instance->getTau() - probabilitySubset;
	constraintValue -= (coeff1 * this->QNodeSolution);

	//if the constraint is violated, ADD THE CONSTRAINT TO THE MODEL
	if (constraintValue < -epsilonValue) {
		return true;
	}
	return false;

}

double MyCallback_PSP::buildCoefficient(vector<int>& complement, double& probabilitySubset, const int& asset) {

	int scenariosToActivate = ceil((1.0 - this->instance->getTau() - probabilitySubset) * static_cast<double>(this->instance->getScenarioNumber()) - epsilonValue);
	//cout << "SCENARIOS ACTIVATE\t" << scenariosToActivate << "PROBABILITY\t" << probabilitySubset << " ASSET\t" << asset << "\nCOMPLEMENT\n";
	vector<pair<double, int>> assetValues(complement.size());
	for (int i = 0; i < complement.size(); i++) {
		//cout << complement.at(i) << " " << this->instance->getAssetPrices().at(asset).at(complement.at(i)) << endl;
		assetValues.at(i) = make_pair(this->instance->getReturns().at(asset).at(complement.at(i)), complement.at(i));
	}
	sort(assetValues.rbegin(), assetValues.rend());

	double coeff = 0;
	for (int i = 0; i < scenariosToActivate; i++) {
		coeff += this->instance->getProbabilities().at(assetValues.at(i).second) * assetValues.at(i).first;
	}
	return coeff;

}

void MyCallback_PSP::getNodeSolutionX_Jacques() {

	this->xNodeSolution = vector<double>(this->instance->getAssetNumber());

	//get solution at the current node of the B&B search tree: x, y and Q
	for (int i = 0; i < this->instance->getAssetNumber(); i++) {
		this->xNodeSolution[i] = getNodeRel(this->x.at(i));
	}

}

void MyCallback_PSP::getNodeSolutionQ_Jacques() {

	this->QNodeSolution = getNodeRel(this->Q);

}

////////////////////////////GET-SET ATTRIBUTES//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////GET///////////////////////////////////////////////////


