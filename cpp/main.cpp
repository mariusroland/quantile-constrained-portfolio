
#include "Common.h"
#include "Util.h"
#include "gurobi_c++.h"
#include "Instance_PSP.h"
#include "ModelSolver_PSP.h"
#include "AdaptiveAlgorithm_PSP.h"

using namespace std;

int Instance_PSP::generatedCuts = 0;
double Instance_PSP::minViolation = INFINITY;
double Instance_PSP::maxViolation = -1;
double Instance_PSP::separationTime = 0.0;
double Instance_PSP::rootNodeGAP = INFINITY;
double Instance_PSP::rootNodeUB = INFINITY;
double Instance_PSP::violationTime = 0.0;

int main(int argc, char* argv[]) {

	user user = user::marius;
	string path;
	vector<string> nameArray;
	string name;

	switch (user)
	{
	case matteo:
	  path = "C:\\Users\\mogier\\Desktop\\RoadefChallenge2020\\Results\\B_set\\"; 
	  path = "C:\\Users\\mogier\\Desktop\\RoadefChallenge2020\\Results\\PSP_Datasets\\Wolsey\\"; 
	  //path = "C:\\Users\\Master\\Desktop\\Roadef\\Instances\\PSP_Datasets\\Beasley\\";
	  name = "A_15";
	  nameArray = { "A_01", "A_02","A_03", "A_04", "A_06","A_07", "A_08", "A_09", "A_10","A_11","A_12", "A_13", "A_14", "A_15"};
	  nameArray = { "B_01","B_02","B_03","B_04","B_05","B_06","B_07","B_08","B_09","B_10","B_11","B_12","B_13" };
	  nameArray = { "C_01","C_02","C_03","C_04","C_05","C_06","C_07","C_08","C_09","C_10","C_11" };
	  nameArray = { "Wolsey_20_200_1","Wolsey_20_200_2","Wolsey_20_200_3","Wolsey_20_200_4","Wolsey_20_200_5","Wolsey_20_200_6","Wolsey_20_200_7","Wolsey_20_200_8","Wolsey_20_200_9","Wolsey_20_200_10","Wolsey_20_200_11","Wolsey_20_200_12",
					"Wolsey_400_200_1","Wolsey_400_200_2","Wolsey_400_200_3","Wolsey_400_200_4","Wolsey_400_200_5","Wolsey_400_200_6","Wolsey_400_200_7","Wolsey_400_200_8","Wolsey_400_200_9","Wolsey_400_200_10","Wolsey_400_200_11","Wolsey_400_200_12"};
	  break;
	case diego:
	  path = "C:\\Users\\cattaruz\\Desktop\\Diego\\Research\\on going\\021_ChallengeRoadef\\instances\\C_set\\";
	  nameArray = {"A_08"};
	  nameArray = { "B_02","B_03","B_04","B_05","B_06","B_07","B_08","B_09","B_10","B_11","B_12","B_13" };
	  break;
	case marius:
	  path = "/home/marius/Documents/trier/competitions/roadef2020/portfolio-code/src/";
	  // nameArray = {"Wolsey_20_200_1","Wolsey_20_200_2","Wolsey_20_200_3","Wolsey_20_200_4","Wolsey_20_200_5","Wolsey_20_200_6",
	  // 			   "Wolsey_20_200_7","Wolsey_20_200_8","Wolsey_20_200_9","Wolsey_20_200_10","Wolsey_20_200_11","Wolsey_20_200_12",
	  // 			   "Wolsey_400_200_1","Wolsey_400_200_2","Wolsey_400_200_3","Wolsey_400_200_4","Wolsey_400_200_5","Wolsey_400_200_6",
	  // 			   "Wolsey_400_200_7","Wolsey_400_200_8","Wolsey_400_200_9","Wolsey_400_200_10","Wolsey_400_200_11","Wolsey_400_200_12"};
	  nameArray = {"Wolsey_20_200_1"};
	//"A_05","A_02","A_15","A_11","A_14","A_08"	
	//"A_08","A_14","A_11","A_15","A_02","A_05"
	  break;
	case server:
	  path = "/home/roland/projects/roadef2020/portfolio-code/src/";
	  nameArray = {"Wolsey_20_200_1","Wolsey_20_200_2","Wolsey_20_200_3","Wolsey_20_200_4","Wolsey_20_200_5","Wolsey_20_200_6",
				   "Wolsey_20_200_7","Wolsey_20_200_8","Wolsey_20_200_9","Wolsey_20_200_10","Wolsey_20_200_11","Wolsey_20_200_12",
				   "Wolsey_400_200_1","Wolsey_400_200_2","Wolsey_400_200_3","Wolsey_400_200_4","Wolsey_400_200_5","Wolsey_400_200_6",
				   "Wolsey_400_200_7","Wolsey_400_200_8","Wolsey_400_200_9","Wolsey_400_200_10","Wolsey_400_200_11","Wolsey_400_200_12"};
	  //"B_15","B_14","B_13","B_12","B_11","B_10","B_09","B_08","B_07","B_06","B_05","B_04","B_03","B_02","B_01"
	  //"B_09","B_08","B_07","B_06","B_05","B_04","B_15","B_14","B_13","B_12","B_11","B_10","B_03","B_02","B_01"	
          //"B_01","B_02","B_03","B_04","B_05","B_06","B_07","B_08","B_09","B_10","B_11","B_12","B_13","B_14","B_15"
	  break;
	default:
	  path = "";
	  nameArray = {"A_14"};
	  break;
	}
	
	//nameArray.clear();
	//Set the batch file reading (to do)...
	  if (argc > 1) {
	  	//cout << "argc = " << argc << endl;
	  	int readIndex = 1;
	  	while (readIndex < argc) {
	  		// cout <<"argv="<<readIndex <<"\t"<< argv[readIndex] << endl;
	  		if ((string)argv[readIndex] == "-name") {
	  			readIndex++;
	  			name = argv[readIndex];
	  			//nameArray.push_back(argv[readIndex]);

	  		}

	  		readIndex++;

	  	}

	  }
	 // cout << endl;

	//COMPUTATION PARAMETERS
	bool isModelReduced = true;

	//bool fixThreads = true;
	bool isAdaptiveAlgorithm = true;
	bool isLogFilePrint = false;

	vector<bool> areWeakVI = { false,true,true };
	vector<bool> areStrongVI = { false,false,true };
	vector<double> timeLimits = { 5400.0,5400.0,5400.0 };

	// vector<double> alphas = { 0,0,0,0.25,0.25,0.25,0.5,0.5,0.5,0.75,0.75,0.75,0,0,0,0.25,0.25,0.25,0.5,0.5,0.5,0.75,0.75,0.75 };
	// vector<int> scenariosToDeactivate = { 15,30,45,15,30,45,15,30,45,15,30,45,15,30,45,15,30,45,15,30,45,15,30,45 };
	vector<double> alphas = { 0 };
	vector<int> scenariosToDeactivate = { 15 };

	//Emptying file if already something there
	string solutionName = "Solutions_Adaptive_POP.csv";
	ofstream mystream;
	mystream.open(solutionName, ofstream::out | ofstream::trunc);
	mystream.close();
	
	for (int fileIndex = 0; fileIndex < nameArray.size(); fileIndex++) {

		Instance_PSP::generatedCuts = 0;
		Instance_PSP::minViolation = INFINITY;
		Instance_PSP::maxViolation = -1;
		Instance_PSP::separationTime = 0.0;
		Instance_PSP::rootNodeGAP = INFINITY;
		Instance_PSP::rootNodeUB = INFINITY;
		Instance_PSP::violationTime = 0.0;

		string fileNumber = nameArray[fileIndex];

		cout << "Setting up file " + fileNumber << "\n";

		Instance_PSP instance(path, fileNumber, isModelReduced, false, false, alphas.at(fileIndex), scenariosToDeactivate.at(fileIndex));
		instance.readFile();

		// try{
		// 	ModelSolver_PSP ms(&instance, 5400.0);
		// 	ms.solve(isLogFilePrint);
		// }
		// catch (GRBException& e) {
		// 	cerr << "Concert exception caught: " << e.getErrorCode() << endl;
		// 	cerr << e.getMessage() << endl;
		// }
		try{
			AdaptiveAlgorithm_PSP adaptiveAlgorithm(&instance, 5400.0);
			adaptiveAlgorithm.solve(isLogFilePrint);
			adaptiveAlgorithm.writeStatistics(solutionName);
		}
		catch (GRBException& e) {
			cerr << "Concert exception caught: " << e.getErrorCode() << endl;
			cerr << e.getMessage() << endl;
		}
		//instance.coeffChecking();
		/*Instance_PSP instGen = Instance_PSP();
		  instGen.WolseyInstanceGenerator(nameArray.at(fileIndex), 400, 200, 0.8, 1.5);*/
		cout<<"Finished " + fileNumber<<endl;
	}

	return 0;
	
}
